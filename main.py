
import socket
import paramiko
import logging as log
from paramiko.ssh_exception import BadHostKeyException, AuthenticationException, SSHException
import sys
import json
import os

module_name="rdo"
module_version = "0.1.0"
commands = ["info", "version", "run"]
configuration_folder=os.path.expanduser("~") + "/.hmgcli.d"
# configfile = '~/.hmgcli.d/'+ module_name+ '.conf'
# configkeys = ["url", "user", "password"]
# cfg = {}

def execute(command,params):
  if command == "run":
    rdo_run(params)
  elif command == "version":
    rdo_version()
  elif command == "info":
    rdo_info(params)

def issue_cmd(ip,port,userid,cmd):
    """Issue command to switch via SSH and return its stdout.

    Args:
        cmd (string): Command to issue.

    Returns:
        string: Command stdout.
    """
    # if self.log_level == self.DEBUG or self.log_level == self.INFO:
    #    paramiko.util.log_to_file(self.SSH_LOG)
    s = paramiko.SSHClient()
    s.load_system_host_keys()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(ip, port, userid)
    stdin, stdout, stderr = s.exec_command(cmd)
    output = stdout.read().decode()
    s.close()
    return output 

def rdo_run(params):
  with open("/Users/rpr/git/pyshell/hosts.json") as json_file:
    data = json.load(json_file)

    for item in data:
      if str(params[0]) == str(item['name']) or str(params[0]) == str("*"):
        print ("Run on: ", str(item['name']))
        # if str(item['name']) == str("tb"):
        ip = item['host']
        port = item['port']
        userid = item['userid']
        # out = issue_cmd(ip,port,userid,params)
        s = paramiko.SSHClient()
        s.load_system_host_keys()
        s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        s.connect(ip, port, userid)
        cparams = params[1:]
        cmd = ''.join(cparams)
        # print ("P1", params[0], "cmd", cmd)
        stdin, stdout, stderr = s.exec_command(cmd)
        out = stdout.read().decode()
        s.close()
        print (out)


## def exec_remote_list_of_cmds(hostname, commands, username, port, sudo=False):
##     client = paramiko.SSHClient()
##     client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
##     client.connect(hostname, port=port, username=username)
##     
##     returned_array = []
##     
##     for command in commands:
##         log.debug('command to launch in ssh in {}: {}'.format(hostname, command))
##         stdin, stdout, stderr = client.exec_command(command)
##         out = stdout.read().decode('utf-8')
##         err = stderr.read().decode('utf-8')
##         returned_array.append({'out': out, 'err': err})
##         log.debug('commnad launched / out: {} / error: {}'.format(out, err))
##     
##     client.close()
## 
##     return returned_array
## 
## retval = exec_remote_list_of_cmds('thebeast02.pretzlaff.info','/bin/ls', 'rpr', '22')
## 
## print ("RetVal: ", retval)


